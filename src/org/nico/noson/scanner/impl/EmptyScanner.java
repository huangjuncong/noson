package org.nico.noson.scanner.impl;

import java.util.Collection;

import org.nico.noson.Noson;
import org.nico.noson.exception.NosonException;
import org.nico.noson.scanner.NoScanner;

/** 
 * 用于接口方法过渡
 * 
 * @author nico
 * @version createTime：2018年4月7日 下午9:29:04
 */

public class EmptyScanner implements NoScanner{

	@Override
	public Noson scanSingle(String json) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<Object> scanArray(String json) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> T scan(String json, Class<T> clazz) throws NosonException {
		throw new UnsupportedOperationException();
	}

}
