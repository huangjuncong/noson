package org.nico.noson.scanner.plant;

import java.util.ArrayList;
import java.util.List;

/** 
 * 
 * @author nico
 * @version createTime：2018年4月9日 下午9:07:08
 */

public class FloatObjArrayPlant extends AbstractPlant{

	@Override
	public List<Float> get() {
		return new ArrayList<Float>();
	}
}
