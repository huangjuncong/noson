package org.nico.noson.scanner;

import java.util.Collection;
import java.util.List;

import org.nico.noson.Noson;
import org.nico.noson.exception.NosonException;

/** 
 * 
 * @author nico
 * @version 创建时间：2017年11月24日 下午9:52:48
 */

public interface NoScanner {

	/**
	 * Scanning the json as single Object & convert to Noson
	 * 
	 * @param json 
	 * 			The json being scanned
	 * @return noson 
	 * 			object
	 */
	public Noson scanSingle(String json);
	
	/**
	 * Scanning the json as array Object & convert to List<Object>
	 * 
	 * @param json 
	 * 			Json being scanned
	 * @return array 
	 * 			Object
	 */
	public Collection<Object> scanArray(String json);
	
	
	public <T> T scan(String json, Class<T> clazz) throws NosonException;
}
