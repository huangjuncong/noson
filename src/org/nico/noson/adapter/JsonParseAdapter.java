package org.nico.noson.adapter;

import org.nico.noson.NosonConfig;
import org.nico.noson.exception.NosonFormatException;
import org.nico.noson.scanner.NoScanner;
import org.nico.noson.util.string.FormatUtils;
import org.nico.noson.util.string.StringUtils;
import org.nico.noson.util.type.TypeUtils;
import org.nico.noson.verify.SymbolVerify;

/** 
 * Noson解析分配中心
 * 
 * @author nico
 * @version 创建时间：2017年11月24日 下午11:15:22
 */

public class JsonParseAdapter {
	
	/**
	 * 验证Json是否合法，并分配扫描器
	 * @param json 要验证的json
	 * @return 返回扫描对象
	 * @throws NosonFormatException
	 */
	public static String adapter(String json) throws NosonFormatException{
		SymbolVerify verify = new SymbolVerify();
		if(StringUtils.isNotBlank(json)){
			json = FormatUtils.formatJson(json);
			if(verify.check(json)){
				if((json.startsWith("{") && json.endsWith("}")) || (json.startsWith("[") && json.endsWith("]"))){
					return json;
				}else{
					throw new NosonFormatException("json str format error !!");
				}
			}else{
				throw new NosonFormatException("json str is not closed !!");
			}
		}else{
			return null;
		}
	}
}
