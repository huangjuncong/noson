package org.nico.noson.test;

import java.util.List;
import java.util.Map;

import org.nico.noson.Noson;
import org.nico.noson.entity.NoType;
import org.nico.noson.test.entity.Nico;

public class TempTest {

	public static void main(String[] args) {
		
		String json = "{list:[{map:{map:{list:[{map:{value:{\"name\":nico,age:21,skill:[java,c,c#,python,php,javascript],deposit:0.0,info:{address:china,job:IT}}}},{map:{value:{\"name\":nico,age:21,skill:[java,c,c#,python,php,javascript],deposit:0.0,info:{address:china,job:IT}}}}]}}},{map:{map:{list:[{map:{value:{\"name\":nico,age:21,skill:[java,c,c#,python,php,javascript],deposit:0.0,info:{address:china,job:IT}}}},{map:{value:{\"name\":nico,age:21,skill:[java,c,c#,python,php,javascript],deposit:0.0,info:{address:china,job:IT}}}}]}}}]}";
//		Map<String, Map[]> target = Noson.convert(json, new NoType<Map<String, Map[]>>(){});
//		System.out.println(target);
//		System.out.println(target.get("list")[1]);
		
		json = "{array:[[{},{}]]]}";
		Map<String, Map[][]> target1 = Noson.convert(json, new NoType<Map<String, Map[][]>>(){});
		System.out.println(target1);
		
	}
}
